var Graph = require("data-structures").Graph;
var fs = require("fs");
var Combinatorics = require('js-combinatorics');
var sw = require("node-stopwatch").Stopwatch;

function buildMap(distances) {
    var map = new Graph();

    for (var i = 0; i < distances.length; i++) {
        var curr = distances[i];

        if (curr) {
            var components = curr.split(" ");
            map.addNode(components[0]);
            map.addNode(components[2]);
            map.addEdge(components[0], components[2], +components[4]);
            map.addEdge(components[2], components[0], +components[4]);
        }
    }

    return map;
}

function measureRoute(map, route) {
    var dist = 0;
    for (var i = 1; i < route.length; i++) {
        var from = route[i - 1];
        var to = route[i];
        var edge = map.getEdge(from, to);
        if (edge) {
            dist += edge.weight;
        } else {
            return;
        }
    }

    return dist;
}

function processFile(data) {
    var distances = ("" + data).split("\r\n");

    var map = buildMap(distances);

    var allDestinations = [];
    map.forEachNode(function (node, id) {
        allDestinations.push(id);
    })

    var allRoutes = Combinatorics.permutation(allDestinations);

    var results = {
        longest: {
            distance: 0
        },
        shortest: {
            distance: 10000
        }
    };

    while (route = allRoutes.next()) {
        var dist = measureRoute(map, route);
        if (dist) {
            if (dist > results.longest.distance) {
                results.longest = {
                    route: route,
                    distance: dist
                };
            } else if (dist < results.shortest.distance) {
                results.shortest = {
                    route: route,
                    distance: dist
                };
            }
        }
    }

    console.log("Longest: " + results.longest.distance);
    console.log("Shortest: " + results.shortest.distance);
}

var stopwatch = sw.create();


fs.readFile("day9-input", function (err, data) {
    if (err) {
        console.log(err);
    } else {
        stopwatch.start();
        processFile(data);
        stopwatch.stop();
        console.log("Completed in " + stopwatch.elapsed.milliseconds + "ms");
    }
});
