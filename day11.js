var sw = require("node-stopwatch").Stopwatch;

var a = 97;
var z = 122;
var bannedLetters = ["i", "o", "l"];
//var input = "cqjxjnds";

String.prototype.replaceCharCodeAt=function(index, charCode) {
    return this.substr(0, index) + String.fromCharCode(charCode) + this.substr(index + 1);
}

function incrementPassword(input) {
    for (var i = input.length - 1; i >= 0; i--) {
        var curr = input.charCodeAt(i);
        if (curr == z) {
            input = input.replaceCharCodeAt(i, a)
        } else {
            input = input.replaceCharCodeAt(i, ++curr);
            break;
        }
    }

    return input;
}

function containsIncreasingStraight(input) {
    for (var i = 1; i < (input.length - 1); i++) {
        var curr = input.charCodeAt(i);
        if ((input.charCodeAt(i - 1) === (curr - 1)) && (input.charCodeAt(i + 1) === (curr + 1))) {
            return true;
        }
    }

    return false;
}

function containsTwoNonOverlappingPairs(input) {
    var firstPairIndex = -1;

    for (var i = 1; i < input.length; i++) {
        if (input[i - 1] == input[i]) {
            if (firstPairIndex === -1) {
                firstPairIndex = i - 1;
                i++;
            } else {
                return true;
            }
        }
    }

    return false;
}

function doesNotContainBannedLetters(input) {
    for (var i = 0; i < bannedLetters.length; i++) {
        if (input.indexOf(bannedLetters[i]) !== -1) {
            return false;
        }
    }

    return true;
}

function isPasswordValid(input) {
    return containsIncreasingStraight(input)
        && doesNotContainBannedLetters(input)
        && containsTwoNonOverlappingPairs(input);
}

function findNextValidPassword(input) {
    while (true) {
        input = incrementPassword(input);
        if (isPasswordValid(input)) {
            return input;
        }
    }
}

var input = "cqjxjnds";

var stopwatch = sw.create();
stopwatch.start();

var next = findNextValidPassword(input);
var nextNext = findNextValidPassword(next);

stopwatch.stop();

console.log(next);
console.log(nextNext);

console.log("Completed in " + stopwatch.elapsed.milliseconds + "ms");
