fs = require("fs");

var disallowedStrings = ["ab", "cd", "pq", "xy"];

function hasAtLeastThreeVowels(input) {
    var vowelCount = 0;

    for (var i = 0; i < input.length; i++) {
        var curr = input[i];
        if (curr === "a" || curr === "e" || curr === "i" || curr === "o" || curr === "u") {
            if (vowelCount == 2) {
                return true;
            }

            vowelCount++;
        }
    }

    return false;
}

function hasALetterThatAppearsTwiceInARow(input) {
    for (var i = 1; i < input.length; i++) {
        if (input[i] === input[i-1]) {
            return true;
        }
    }

    return false;
}

function doesNotContainADisallowedString(input) {
    for (var i = 0; i < disallowedStrings.length; i++) {
        if (input.indexOf(disallowedStrings[i]) !== -1) {
            return false;
        }
    }

    return true;
}

function isNice1(input) {
    return hasAtLeastThreeVowels(input) && hasALetterThatAppearsTwiceInARow(input) && doesNotContainADisallowedString(input);
};

function containsRepeatedPair(input) {
    for (var i = 0; i < input.length - 2; i++) {
        var pair = input[i] + input[i + 1];

        var position = input.indexOf(pair);
        if (position === i || position === (i + 1) || position === (i - 1)) {
            position = input.indexOf(pair, i + 2);
        }

        if (position !== - 1) {
            return true;
        }
    }

    return false;
}

function containsRepeatedLetterWithASingleLetterBetween(input) {
    for (var i = 2; i < input.length; i++) {
        if (input[i - 2] === input[i]) {
            return true;
        }
    }

    return false;
}

function isNice2(input) {
    return containsRepeatedLetterWithASingleLetterBetween(input)
        && containsRepeatedPair(input);
}

function processFile(data) {
    var allStrings = ("" + data).split("\n");

    var nice1Count = 0;
    var nice2Count = 0;

    for (var i = 0; i < allStrings.length; i++) {
        if (isNice1(allStrings[i])) {
            nice1Count++;
        }
        if (isNice2(allStrings[i])) {
            nice2Count++;
        }
    }

    return [nice1Count, nice2Count];
}

fs.readFile("day5-input", function (err, data) {
    if (err) {
        console.log(err);
    } else {
        var result = processFile(data);
        console.log("Part 1: The file contains " + result[0] + " nice strings");
        console.log("Part 2: The file contains " + result[1] + " nice strings");
    }
});
