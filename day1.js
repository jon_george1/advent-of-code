var data = process.argv[2];

if (data && data.length > 0) {
    var floor = 0, basementIndex = 0;
    for (var i = 0; i < data.length; i++) {
        var instruction = data[i];
        if (instruction === "(") {
            floor++;
        } else if (instruction === ")") {
            floor--;
        }

        if (floor === -1 && basementIndex === 0) {
            basementIndex = i + 1;
        }
    }

    console.log("Final floor " + floor);
    console.log("Enters the basement at instruction " + basementIndex);
} else {
    console.log("Floor 0");
    console.log("Never enters the basement");
}
