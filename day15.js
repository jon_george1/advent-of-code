var input = {
    Sprinkles: {
        capacity: 5,
        durability: -1,
        flavor: 0,
        texture: 0,
        calories: 5
    },
    PeanutButter: {
        capacity: -1,
        durability: 3,
        flavor: 0,
        texture: 0,
        calories: 1
    },
    Frosting: {
        capacity: 0,
        durability: -1,
        flavor: 4,
        texture: 0,
        calories: 6
    },
    Sugar: {
        capacity: -1,
        durability: 0,
        flavor: 0,
        texture: 2,
        calories: 8
    }
};

//var input = {
//    Butterscotch: {
//        capacity: -1,
//        durability: -2,
//        flavor: 6,
//        texture: 3,
//        calories: 8
//    },
//    Cinnamon: {
//        capacity: 2,
//        durability: 3,
//        flavor: -2,
//        texture: -1,
//        calories: 3
//    }
//}

var data = [input.Sprinkles, input.PeanutButter, input.Frosting, input.Sugar];
//var data = [input.Butterscotch, input.Cinnamon];

var quantities = [];

//quantities.push([44, 56]);

for (var i0 = 0; i0 <= 100; i0++) {
    for (var i1 = 0; i1 <= 100; i1++) {
        for (var i2 = 0; i2 <= 100; i2++) {
            for (var i3 = 0; i3 <= 100; i3++) {
                if (i0 + i1 + i2 + i3 === 100) {
                    quantities.push([i0, i1, i2, i3]);
                }
            }
        }
    }
}

var currentHighest = 0;

// Total quantities
for (var measurement = 0; measurement < quantities.length; measurement++) {
    var curr = quantities[measurement];

    var result = {
        capacity: 0,
        durability: 0,
        flavor: 0,
        texture: 0,
        calories: 0
    };

    for (var i = 0; i < curr.length; i++) {
        result.capacity += curr[i] * data[i].capacity;
        result.durability += curr[i] * data[i].durability;
        result.flavor += curr[i] * data[i].flavor;
        result.texture += curr[i] * data[i].texture;
        result.calories += curr[i] * data[i].calories
    }

    if (result.calories === 500) {
        var finalResult = Math.max(result.capacity, 0) * Math.max(result.durability, 0) * Math.max(result.flavor, 0) * Math.max(result.texture, 0);

    //    console.log(result);
    //    console.log(finalResult);

        currentHighest = Math.max(finalResult, currentHighest);
    }
}

console.log(currentHighest);
