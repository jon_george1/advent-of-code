var fs = require("fs");

function processFile (input) {
    // Tweak the file into JSON...
    var sues = [];
    var lines = input.split("\r\n");

    for (var i = 0; i < lines.length; i++) {
        var components = lines[i].split[" "];

        var sue = {
            number: +components[1].substring(0, components[1].length - 1)
        };

        for (var i = 2; i < components.length; i += 2) {
            var prop = components[i].replace(":", "");
            var val = components[i + 1].replace(",", "");
            sue[prop] = val;
        }
    }

    console.log(sues);
}


fs.readFile("day16-input", function (err, data) {
    if (err) {
        console.log(err);
    } else {
        processFile("" + data);
    }
});
