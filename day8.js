fs = require("fs");

function encodeString(input) {
    var output = "\"";

    for (var i = 0; i < input.length; i++) {
        switch (input[i]) {
            case "\"":
                output += "\\\"";
                break;
            case "\\":
                output += "\\\\";
                break;
            default:
                output += input[i];
                break;
        }
    }

    output += "\"";

    return output;
}

function processFile(data) {
    var codeLength = 0;
    var memLength = 0;
    var encodedCodeLength = 0;

    var instructions = ("" + data).split("\r\n");

    for (var i = 0; i < instructions.length; i++) {
        var curr = instructions[i];

        if (curr) {
            codeLength += curr.length;

            memLength += eval(curr + ".length");

            encodedCodeLength += encodeString(curr).length;
        }
    }

    return {
        part1: codeLength - memLength,
        part2: encodedCodeLength - codeLength
    }
}


fs.readFile("day8-input", function (err, data) {
    if (err) {
        console.log(err);
    } else {
        var result = processFile(data);
        console.log("Part 1: " + result.part1);
        console.log("Part 2: " + result.part2);
    }
});
