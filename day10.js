var sw = require("node-stopwatch").Stopwatch;

function lookAndSay(input) {
    var current = input[0];
    var count = 1;
    var result = "";

    for (var i = 1; i < input.length; i++) {
        if (input[i] == current) {
            count++;
        } else {
            result += count;
            result += current;

            current = input[i];
            count = 1;
        }
    }

    result += count;
    result += current;

    return result;
}

function processInput(input, iterations, verbose) {
    var stopwatch = sw.create();
    stopwatch.start();

    var current = input;

    for (var i = 0; i < iterations; i++) {
        current = lookAndSay(current);
    }

    stopwatch.stop();

    if (verbose) {
        console.log("Result from input " + input + " after " + iterations + " iterations is " + current.length);
        console.log("Completed in " + stopwatch.elapsed.milliseconds + "ms");
    } else {
        console.log(iterations + "\t" + current.length);
    }
}

processInput("1321131112", 40, true);
processInput("1321131112", 50, true);
