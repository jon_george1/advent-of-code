var fs = require("fs");
var PNG = require("node-png").PNG;

var lightingGrid = (function () {
    function getIndex(x, y) {
        return (1000 * y) + x;
    };

    var constr = function () {
        this.grid = [];

        for (var i = 0; i < 1000000; i++) {
            this.grid[i] = false;
        }
    };

    constr.prototype.process = function (from, to, op) {
        for (var x = from.x; x <= to.x; x++) {
            for (var y = from.y; y <= to.y; y++) {
                var i = getIndex(x, y)
                this.grid[i] = op(this.grid[i]);
            }
        }
    }

    constr.prototype.toggle = function (from, to) {
        this.process(from, to, function (input) {
            return !input;
        });
    };

    constr.prototype.on = function (from, to) {
        this.process(from, to, function (input) {
            return true;
        });
    };

    constr.prototype.off = function (from, to) {
        this.process(from, to, function (input) {
            return false;
        });
    };

    constr.prototype.countLit = function () {
        var count = 0;
        for (var i = 0; i < this.grid.length; i++) {
            if (this.grid[i]) {
                count++;
            }
        }

        return count;
    };

    constr.prototype.exportTo = function (file) {
        var result = new PNG({
            height: 1000,
            width: 1000
        });

        for (var i = 0; i < this.grid.length; i++) {
            var pngpos = i * 4;
            var col = this.grid[i] ? 255 : 0;

            result.data[pngpos] = col;
            result.data[pngpos + 1] = col;
            result.data[pngpos + 2] = col;
            result.data[pngpos + 3] = 255;
        }

        result.pack().pipe(fs.createWriteStream(file));
    };

    return constr;
})();

var lightingGrid2 = (function () {
    function getIndex(x, y) {
        return (1000 * y) + x;
    };

    var constr = function () {
        this.grid = [];

        for (var i = 0; i < 1000000; i++) {
            this.grid[i] = 0;
        }
    };

    constr.prototype.process = function (from, to, op) {
        for (var x = from.x; x <= to.x; x++) {
            for (var y = from.y; y <= to.y; y++) {
                var i = getIndex(x, y)
                this.grid[i] = op(this.grid[i]);
            }
        }
    }

    constr.prototype.toggle = function (from, to) {
        this.process(from, to, function (input) {
            return input + 2;
        });
    };

    constr.prototype.on = function (from, to) {
        this.process(from, to, function (input) {
            return input + 1;
        });
    };

    constr.prototype.off = function (from, to) {
        this.process(from, to, function (input) {
            return Math.max(input - 1, 0);
        });
    };

    constr.prototype.countBrightness = function () {
        var brightness = 0;
        for (var i = 0; i < this.grid.length; i++) {
            brightness += this.grid[i];
        }

        return brightness;
    };

    constr.prototype.exportTo = function (file) {
        var result = new PNG({
            height: 1000,
            width: 1000
        });

        for (var i = 0; i < this.grid.length; i++) {
            var pngpos = i * 4;

            result.data[pngpos] = 255;
            result.data[pngpos + 1] = 0;
            result.data[pngpos + 2] = 0;
            result.data[pngpos + 3] = Math.min((2 * this.grid[i]), 255);
        }

        result.pack().pipe(fs.createWriteStream(file));
    };

    return constr;
})();

var instruction = (function () {
    function processCoords(input) {
        var s = input.split(",");
        return {
            x: +s[0],
            y: +s[1]
        };
    }

    var constr = function (input) {
        var coords;

        if (input.indexOf("turn on") === 0) {
            this.operation = "on";
            coords = input.slice(8)
        } else if (input.indexOf("turn off") === 0) {
            this.operation = "off";
            coords = input.slice(9)
        } else {
            this.operation = "toggle";
            coords = input.slice(7)
        }

        coords = coords.split(" through ");
        this.from = processCoords(coords[0]);
        this.to = processCoords(coords[1]);
    };

    constr.prototype.log = function () {
        var result;
        if (this.operation === "on") {
            result = "turn on ";
        } else if (this.operation === "off") {
            result = "turn off ";
        } else {
            result = "toggle ";
        }

        result += this.from.x;
        result += ",";
        result += this.from.y;

        result += " through ";

        result += this.to.x;
        result += ",";
        result += this.to.y;

        console.log(result);
    };

    constr.prototype.execute = function (grid) {
        grid[this.operation](this.from, this.to);
    };

    return constr;
})();

function processFile(data) {
    var allStrings = ("" + data).split("\n");

    var grid = new lightingGrid();
    var grid2 = new lightingGrid2();

    for (var i = 0; i < allStrings.length; i++) {
        if (allStrings[i]) {
            var curr = new instruction(allStrings[i]);
//            curr.log();
            curr.execute(grid);
            curr.execute(grid2);
        }
    }

    grid.exportTo("day6-output.png");
    grid2.exportTo("day6-2-output.png");

    return {
        part1: grid.countLit(),
        part2: grid2.countBrightness()
    };
}

fs.readFile("day6-input", function (err, data) {
    if (err) {
        console.log(err);
    } else {
        var result = processFile(data);
        console.log("Part 1: There are " + result.part1 + " lit bulbs");
        console.log("Part 2: Total brightness is " + result.part2);
    }
});
