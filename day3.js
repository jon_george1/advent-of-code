fs = require("fs");

var fileName = process.argv[2];

var santaTracker = (function () {

  var constr = function () {
    this.grid = { "0-0": 1 };
    this.x = 0;
    this.y = 0;
  };

  constr.prototype.move = function (direction) {
    switch (direction) {
      case "^":
        this.y++;
        break;
      case "v":
        this.y--;
        break;
      case "<":
        this.x--;
        break;
      case ">":
        this.x++;
        break;
      default:
        return;
    }

    var key = this.x + "-" + this.y;
    this.grid[key] = this.grid[key] || 0;
    this.grid[key]++;
  }

  constr.prototype.countHousesWithPresents = function() {
    var result = 0;
    for (house in this.grid) {
      result++;
    }

    return result;
  }

  constr.prototype.getGrid = function () {
    return this.grid;
  }

  return constr;
})();

function processForSingleSanta(data) {
  var tracker = new santaTracker();

  var instructions = "" + data;

  for (var i = 0; i < instructions.length; i++) {
    tracker.move(instructions[i]);
  }

  return tracker.countHousesWithPresents();
}

function processForMultipleSantas(data, santaCount) {
  var santas = [];

  for (var i = 0; i < santaCount; i++) {
    santas.push(new santaTracker());
  }

  var currentSanta = 0;

  var instructions = "" + data;

  for (var i = 0; i < instructions.length; i++) {
    santas[currentSanta].move(instructions[i]);
    currentSanta = (currentSanta + 1) % santaCount;
  }


  var combinedGrid = {};
  for (var i = 0; i < santaCount; i++) {
    var grid = santas[i].getGrid();
    for (house in grid) {
      combinedGrid[house] = (combinedGrid[house] || 0) + grid[house];
    }
  }

  // Now final count of the houses with presents
  var result = 0;
  for (house in combinedGrid) {
    result++;
  }

  return result;
}

fs.readFile(fileName, function (err, data) {
  if (err) {
    console.log(err);
  } else {
    var singleSantaResults = processForSingleSanta(data);
    console.log("With one santa, there are " + singleSantaResults + " houses that get at least one present");

    var twoSantaResults = processForMultipleSantas(data, 2);
    console.log("With two santas, there are " + twoSantaResults + " houses that get at least one present");

    var threeSantaResults = processForMultipleSantas(data, 3);
    console.log("With three santas, there are " + threeSantaResults + " houses that get at least one present");

    var fourSantaResults = processForMultipleSantas(data, 4);
    console.log("With four santas, there are " + fourSantaResults + " houses that get at least one present");
  }
});
