var sw = require("node-stopwatch").Stopwatch;
var fs = require("fs");
var Combinatorics = require("js-combinatorics");

var stopwatch = sw.create();

function parse(input) {
    var instructions = input.split("\r\n");

    var data = {};

    for (var i = 0; i < instructions.length; i++) {
        if (instructions[i]) {
            var curr = instructions[i].split(" ");
            var name = curr[0];
            data[name] = data[name] || { };
            var points = +curr[3];

            var other = curr[10].slice(0, -1);
            if (curr[2] === "gain") {
                data[name][other] = points;
            } else {
                data[name][other] = -points;
            }
        }
    }

    return data;
}

function extractNames(data) {
    var names = [];

    for (prop in data) {
        names.push(prop);
    }

    return names;
}

function calculateValue(arrangement, data) {
    var result = {};
    var total = 0;
    for (var i = 0; i < arrangement.length; i++) {
        var curr = data[arrangement[i]];
        var left = curr[arrangement[i === 0 ? arrangement.length - 1 : i - 1]];
        var right = curr[arrangement[i === arrangement.length - 1 ? 0 : i + 1]];
        var currResult = {
            left: left,
            right: right,
            total: left + right
        };

        total += currResult.total;
        result[arrangement[i]] = currResult;
    }

    result.total = total;
    return result;
}

function scoreArrangements(data, arrangements) {
    var results = [];

    while (arrangement = arrangements.next()) {
        results.push(calculateValue(arrangement, data));
    }

    return results;
}

function extractBestScore(results) {
    var scores = results.map(function (curr, i, arr) {
        return curr.total;
    }).filter(function (val, idx, arr) {
        return arr.indexOf(val) === idx;
    });

    return Math.max.apply(Math, scores);
}

function process(input) {
    var data = parse(input);
    var names = extractNames(data);

    var arrangements = Combinatorics.permutation(names);

    var results = scoreArrangements(data, arrangements);
    var best = extractBestScore(results);

    console.log("Part 1: " + best);

    // Now do part 2
    for (person in data) {
        data[person]["Jon"] = 0;
    }

    data["Jon"] = names.reduce(function (prev, curr, i, arr) {
        prev[curr] = 0;
        return prev;
    }, { })

    names.push("Jon");

    arrangements = Combinatorics.permutation(names);

    results = scoreArrangements(data, arrangements);
    best = extractBestScore(results);

    console.log("Part 2: " + best);
}

fs.readFile("day13-input", function (err, data) {
    if (err) {
        console.log(err);
    } else {
        stopwatch.start();
        process("" + data);

        stopwatch.stop();
        console.log("Completed in " + stopwatch.elapsed.milliseconds + "ms");
    }
});
