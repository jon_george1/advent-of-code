var sw = require("node-stopwatch").Stopwatch;
var fs = require("fs");

var stopwatch = sw.create();

//function part1(input) {
//    var pattern = /-?\d+/g;
//    var numbers = input.match(pattern);
//
//    var sum = numbers.reduce(function (prev, curr, idx, arr) {
//        return prev + +curr;
//    }, 0)
//    console.log("Part 1: " + sum);
//
//}

function processObject(input, skipRed) {
    // What type of thing is this?
    if (input == null) {
        return 0;
    }

    if (typeof input === "number") {
        return input;
    }

    if (typeof input === "object") {
        var result = 0;
        var isArray = Array.isArray(input);
        for (prop in input) {

            // Check: if the property value is red, and this is not an array, then return 0
            if (skipRed && !isArray && input[prop] === "red") {
                return 0;
            }

            result += processObject(input[prop], skipRed);
        }

        return result;
    }

    return 0;
}

function process(input) {
    var data = JSON.parse(input);
    console.log("Part 1: " + processObject(data, false));
    console.log("Part 2: " + processObject(data, true));
}

//process("[1,2,3]");
//process("[1,{\"c\":\"red\",\"b\":2},3]");
//process("{\"d\":\"red\",\"e\":[1,2,3,4],\"f\":5}");
//process("[1,\"red\",5]")

fs.readFile("day12-input", function (err, data) {
    if (err) {
        console.log(err);
    } else {
        stopwatch.start();
        process("" + data);

        stopwatch.stop();
        console.log("Completed in " + stopwatch.elapsed.milliseconds + "ms");
    }
});
