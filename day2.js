fs = require("fs");

var fileName = process.argv[2];

function compareNumbers(a, b) {
  return a - b;
}

function getDimensions(input) {
	var split = ("" + input).split("x");
	return {
		l: +split[0],
		w: +split[1],
		h: +split[2],
	}
}

function calculateRequiredPaper(d) {
	return (2 * d.l * d.w)
		+ (2 * d.w * d.h)
		+ (2 * d.h * d.l)
		+ Math.min(d.l * d.w, d.l * d.h, d.w * d.h);
}

function calculatedRequiredRibbon(d) {
	var arr = [d.l, d.w, d.h].sort(compareNumbers);
	var perimeter = (arr[0] * 2) + (arr[1] * 2);
	var volume = arr[0] * arr[1] * arr[2];

	return perimeter + volume;
}

function processInput(data) {
	var parcels = ("" + data).split("\n");
	var paperRequired = 0;
	var ribbonRequired = 0;

	for (var i = 0; i < parcels.length; i++) {
		var d = getDimensions(parcels[i]);
		paperRequired += calculateRequiredPaper(d);
		ribbonRequired += calculatedRequiredRibbon(d);
	}

	return {
		paper: paperRequired,
		ribbon: ribbonRequired
	};
}

fs.readFile(fileName, function (err, data) {
	if (err) {
		console.log(err);
	} else {
		var amounts = processInput(data);
		console.log("The elves require " + amounts.paper + " square feet of paper and " + amounts.ribbon + " feet of ribbon");
	}
});