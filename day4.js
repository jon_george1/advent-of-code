var md5 = require("md5");
var sw = require("node-stopwatch").Stopwatch;

var current = 1;
var key = "bgvyzdsv";
var stopwatch = sw.create();

stopwatch.start();
do {
	var hash = md5(key + current);
	if (hash.indexOf("000000") === 0) {
		break;
	}
	current++;
} while (true);

stopwatch.stop();

console.log("Answer is " + current + ", took " + stopwatch.elapsed.milliseconds);