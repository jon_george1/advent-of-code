var sw = require("node-stopwatch").Stopwatch;
var fs = require("fs");

var stopwatch = sw.create();

function buildReindeerData (descriptions) {
    var reindeer = [];

    for (var i = 0; i < descriptions.length; i++) {
        if (descriptions[i]) {
            var description = descriptions[i].split(" ");
            var newDeer = {
                name: description[0],
                speed: +description[3],
                flyingTime: +description[6],
                restTime: +description[13],
            };
            newDeer.iterationTime = newDeer.flyingTime + newDeer.restTime;
            newDeer.iterationDistance = newDeer.speed * newDeer.flyingTime;
            reindeer.push(newDeer);
        }
    }

    return reindeer;
}

function getDistance(reindeer, time) {
    var fullIterations = Math.floor(time / reindeer.iterationTime);
    var distance = fullIterations * reindeer.iterationDistance;
    var remainingFlightTime = Math.min(reindeer.flyingTime, time % reindeer.iterationTime);
    distance += (remainingFlightTime * reindeer.speed);
    return distance;
}

function race1(reindeer, time) {
    var distances = [];

    for (var i = 0; i < reindeer.length; i++) {
        distances.push(getDistance(reindeer[i], time));
    }

    return distances;
}

function race2(reindeer, time) {
    var scores = {};
    for (var i = 0; i < reindeer.length; i++) {
        scores[reindeer[i].name] = 0;
    }

    for (var i = 1; i <= time; i++) {
        var distances = race1(reindeer, i);
        var max = Math.max.apply(Math, distances);
        var idx = distances.indexOf(max);

        scores[reindeer[idx].name]++;
    }

    return scores;
}

function processFile (data) {
    var descriptions = ("" + data).split("\r\n");
    var reindeer = buildReindeerData(descriptions);

    var distances = race1(reindeer, 2503)
    console.log("Race 1: The winning reindeer has travelled " + Math.max.apply(Math, distances));

    var scores = race2(reindeer, 2503);
    console.log("Scores for race 2:")
    console.log(scores);
}

fs.readFile("day14-input", function (err, data) {
    if (err) {
        console.log(err);
    } else {
        stopwatch.start();
        processFile(data);
        stopwatch.stop();
        console.log("Completed in " + stopwatch.elapsed.milliseconds + "ms");
    }
});
