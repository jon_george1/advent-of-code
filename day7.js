var Graph = require("data-structures").Graph;
fs = require("fs");

var operations = {
    and: function (x, y) {
        return x & y;
    },
    or: function (x, y) {
        return x | y;
    },
    not: function (x) {
        return ~x & 65535;
    },
    lshift: function (x, bits) {
        return (x << bits) & 65535 ;
    },
    rshift: function (x, bits) {
        return (x >> bits) & 65535;
    },
    noop: function (x) {
        return x;
    }
};

Graph.prototype.addOrGetNode = function (id, signal) {
    var node = this.addNode(id) || this.getNode(id);
    if (signal != undefined) {
        node.signal = +signal;
    }

    //    console.log("Added node " + id + " with signal " + signal);
    return node;
}

Graph.prototype.addGateEdge = function (fromId, toId, gate, param) {
    var from = this.addOrGetNode(fromId);
    var to = this.addOrGetNode(toId);

    var edge = this.addEdge(fromId, toId);
    edge.gate = gate;
    edge.fromId = fromId;
    edge.toId = toId
    edge.param = +param;

    return edge;
}

function addNotToCircuit(circuit, components) {
    circuit.addGateEdge(components[1], components[3], "not");
}

function addAndToCircuit(circuit, components) {
    var in1 = components[0];
    var in2 = components[2];
    var out = components[4];

    if (!isNaN(+in1)) {
        // First input is numeric; add a single edge with the value as an extra parameter
        circuit.addGateEdge(components[2], components[4], "and", +in1);
    } else if (!isNaN(+in2)){
        circuit.addGateEdge(components[0], components[4], "and", +in2);
    } else {
        circuit.addGateEdge(components[0], components[4], "and");
        circuit.addGateEdge(components[2], components[4], "and");
    }
}

function addOrToCircuit(circuit, components) {
    var in1 = components[0];
    var in2 = components[2];
    var out = components[4];

    if (!isNaN(+in1)) {
        // First input is numeric; add a single edge with the value as an extra parameter
        circuit.addGateEdge(components[2], components[4], "or", +in1);
    } else if (!isNaN(+in2)){
        circuit.addGateEdge(components[0], components[4], "or", +in2);
    } else {
        circuit.addGateEdge(components[0], components[4], "or");
        circuit.addGateEdge(components[2], components[4], "or");
    }
}

function addLShiftToCircuit(circuit, components) {
    circuit.addGateEdge(components[0], components[4], "lshift", components[2]);
}

function addRShiftToCircuit(circuit, components) {
    circuit.addGateEdge(components[0], components[4], "rshift", components[2]);
}

function addSetToCircuit(circuit, components) {
    // Is the first component a number?
    if (isNaN(+components[0])) {
        circuit.addGateEdge(components[0], components[2], "noop");
    } else {
        circuit.addOrGetNode(components[2], components[0]);
    }
}

function reset(circuit) {
    circuit.forEachNode (function (node, id) {
        if (circuit.getInEdgesOf(id).length !== 0) {
            delete node.signal;
        }
    })
}

function getSignal(circuit, nodeId) {
    var node = circuit.getNode(nodeId);

    if (node.signal === undefined) {
        var inputs = circuit.getInEdgesOf(nodeId);

        var inputSignals = [];
        for (var i = 0; i < inputs.length; i++) {
            var inputSignal = getSignal(circuit, inputs[i].fromId);
            inputSignals.push(inputSignal);
        }

        if (inputs[0].param) {
            inputSignals.push(inputs[0].param);
        }

        node.signal = operations[inputs[0].gate].apply(null, inputSignals);
    }

    return node.signal;
}

function processInstruction(circuit, instruction) {
    if (!instruction) {
        return;
    }

    var components = instruction.split(" ");

    if (components[0] === "NOT") {
        addNotToCircuit(circuit, components);
    } else if (components[1] === "AND") {
        addAndToCircuit(circuit, components);
    } else if (components[1] === "OR") {
        addOrToCircuit(circuit, components);
    } else if (components[1] === "LSHIFT") {
        addLShiftToCircuit(circuit, components);
    } else if (components[1] === "RSHIFT") {
        addRShiftToCircuit(circuit, components);
    } else {
        addSetToCircuit(circuit, components);
    }
}

function processFile(data) {
    var circuit = new Graph();

    var instructions = ("" + data).split("\r\n");

    for (var i = 0; i < instructions.length; i++) {
        processInstruction(circuit, instructions[i]);
    }

    return circuit;
}

fs.readFile("day7-input", function (err, data) {
    if (err) {
        console.log(err);
    } else {
        var circuit = processFile(data);
        var part1 = getSignal(circuit, "a");
        console.log("Part 1: Signal at a is " + part1);
        reset(circuit);
        circuit.getNode("b").signal = part1;
        var part2 = getSignal(circuit, "a");
        console.log("Part 2: Signal at a is " + part2);    }
});
